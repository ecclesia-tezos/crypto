import os
import sys

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

def encrypt(message: bytes, seconds: int, squarings_per_second: int):
    if not seconds or not squarings_per_second:
        raise AssertionError

    # hard code safe exponent to use
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

    # see RSA for security specifications
    p, q = private_key.private_numbers().p, private_key.private_numbers().q
    n = private_key.public_key().public_numbers().n
    phi_n = (p - 1) * (q - 1)

    # Fernet is an asymmetric encryption protocol using AES
    key = Fernet.generate_key()
    key_int = int.from_bytes(key, sys.byteorder)
    cipher_suite = Fernet(key)

    # Vote Encryption
    encrypted_message = cipher_suite.encrypt(message)

    # Pick safe, pseudo-random a where 1 < a < n
    # Alternatively, we could use a = 2
    a = int.from_bytes(os.urandom(32), sys.byteorder) % n + 1

    # Key Encryption
    t = seconds * squarings_per_second
    e = 2**t % phi_n
    b = fast_exponentiation(n, a, e)

    encrypted_key = (key_int % n + b) % n
    return p, q, n, a, t, encrypted_key, encrypted_message, key_int

def decrypt(n: int, a: int, t: int, enc_key: int, enc_message: int) -> bytes:
    # Successive squaring to find b
    # We assume this cannot be parallelized
    b = a % n
    for i in range(t):
        b = b**2 % n
    dec_key = (enc_key - b) % n

    # open the message with the key
    return open_message(dec_key, enc_message)

def open_message(key: int, enc_message: int) -> bytes:
    # decrypt message given the key
    key_bytes = int.to_bytes(key, length=64, byteorder=sys.byteorder)
    cipher_suite = Fernet(key_bytes)
    return cipher_suite.decrypt(enc_message)

def fast_exponentiation(n: int, g: int, x: int) -> int:
    # reverses binary string
    binary = bin(x)[2:][::-1]
    squares = successive_squares(g, n, len(binary))
    # keeps positive powers of two
    factors = [tup[1] for tup in zip(binary, squares) if tup[0] == '1']
    acc = 1
    for factor in factors:
        acc = acc * factor % n
    return acc


def successive_squares(base: int, mod: int, length: int) -> [int]:
    table = [base % mod]
    prev = base % mod
    for n in range(1, length):
        squared = prev**2 % mod
        table.append(squared)
        prev = squared
    return table
