from .timelock_encryption import encrypt, decrypt, open_message

class TimelockedCandidate:
    """
        Class that serves as an adapter to the timelock encryption suite.
        Provides serialization/deserialization as well as methods to
        encrypt, decrypt (brute force TLE) and open (with voluntarily provided key).
    """
    def __init__(self, n: int, a: int, t: int, enc_key: int, enc_candidate: int, key: int = 0):
        """
            Class constructor with all variables needed to brute force the timelock puzzle
            The key is 0 by default, because it might not have been revealed. In which case
            brute forcing would be the only way.
        """
        self.n = n
        self.a = a
        self.t = t
        self.enc_key = enc_key
        self.enc_candidate = enc_candidate
        self.key = key

    def serialize(self) -> str:
        """ Serializes the public part of the TLE into a hex string and the key into string """
        string = f'{self.n}|{self.a}|{self.t}|{self.enc_key}|{self.enc_candidate.hex()}'
        return string.encode('utf-8').hex(), str(self.key)

    def decrypt(self) -> str:
        """ Decrypts a timelock encrypted candidate into a string by brute force """
        candidate_bytes = decrypt(self.n, self.a, self.t, self.enc_key, self.enc_candidate)
        return candidate_bytes.decode()

    def open_candidate(self, key: str) -> str:
        """ Opens a timelock encrypted candidate into string using the revealed key """
        candidate_bytes = open_message(int(key), self.enc_candidate)
        return candidate_bytes.decode()

    @classmethod
    def encrypt(cls, candidate: str):
        """ Uses the timelock suite to come up with an object """
        p, q, n, a, t, encr_key, enc_candidate, key = encrypt(
            candidate.encode(),
            1,
            50000
        )
        return cls(n, a, t, encr_key, enc_candidate, key)

    @classmethod
    def deserialize(cls, serialization: bytes):
        """ Deserializes the string produced by the serialize method """
        n, a, t, e_key, e_candidate = bytes.fromhex(serialization).decode('utf-8').split('|')
        return cls(int(n), int(a), int(t), int(e_key), bytes.fromhex(e_candidate))
