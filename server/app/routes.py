import os
from flask import jsonify
from flask import request
from flask import abort
from app import app
import boto3

import requests
from markupsafe import escape

from timelock.timelocked_candidate import TimelockedCandidate

@app.route('/tzstats/<path:subpath>')
def tzstats(subpath):
    tzstats_path = escape(subpath)
    tzstats_params = request.query_string.decode('utf-8')
    tzstats_url= "https://api.carthagenet.tzstats.com/%s?%s" % (tzstats_path, tzstats_params)
    response = requests.get(tzstats_url)
    return response.text

@app.route('/mint', methods=['POST'])
def mint():
    if not authenticate(request):
        return abort(403)

    modulus = request.json.get('modulus')
    stream = os.popen("/app/zkp/build/mint_credentials -m %s" % modulus)
    public_value = stream.readline().strip()
    serial_number = stream.readline().strip()
    randomness = stream.readline().strip()
    return jsonify(
        public_value=public_value,
        serial_number=serial_number,
        randomness=randomness
    )

@app.route('/vote', methods=['POST'])
def vote():
    modulus = request.json.get('modulus')
    serial_number = request.json.get('serial_number')
    randomness = request.json.get('randomness')
    credential = request.json.get('credential')
    credentials = request.json.get('credentials')
    candidate = request.json.get('candidate')
    encrypted_candidate, key = TimelockedCandidate.encrypt(candidate).serialize()
    return jsonify(vote=encrypted_candidate, key=key)

@app.route('/verify', methods=['POST'])
def verify():
    modulus = request.json.get('modulus')
    ballots = request.json.get('ballots')
    credentials = request.json.get('credentials')

    verified_ballots = list(
        map(lambda ballot: verify_ballot(ballot, credentials, modulus), ballots)
    )
    return jsonify(verified_ballots=verified_ballots)

def verify_ballot(ballot, credentials, modulus):
    vote = ballot.get('voteCommitment')
    key = ballot.get('voteOpening')
    encrypted_candidate = TimelockedCandidate.deserialize(vote)
    try:
        candidate = encrypted_candidate.open_candidate(key)
        return { 'is_valid': True, 'candidate': candidate }
    except:
        return { 'is_valid': False, 'candidate': '' }

def credentials_to_arguments(credentials):
    return " -a ".join(credentials)

def authenticate(request):
    email = request.json.get('email')
    auth_token = request.json.get('token')
    dynamodb = dynamodb_resource()
    table = dynamodb.Table('VoterAuthenticationTokens')
    response = table.get_item(Key={ 'VoterId': email })
    if 'Item' in response:
        return response['Item']['VoterToken'] == auth_token
    else:
        return False

def dynamodb_resource():
    session = boto3.session.Session()
    if os.environ['ENVIRONMENT'] == 'development':
        return session.resource('dynamodb', endpoint_url='http://localhost:8000', region_name='eu-west-2')
    elif os.environ['ENVIRONMENT'] == 'production':
        return session.resource('dynamodb', region_name='eu-west-2')
