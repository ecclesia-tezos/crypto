# Developer Guide
## Developing
### Docker
To build the project, you need a working docker installation. Docker needs to be callable without super user permissions for the scripts to work out of the box. See how to install Docker [here](https://docs.docker.com/install/).

### Building
After pulling the repository, cd into it and run
docker build -t crypto .

This step will create a docker image called crypto.
### Generating a modulus
After building the image, you can use it to create a safe modulus of desired length as such:

`docker run crypto:latest ./generate_modulus -b 3072`

### Minting credentials
After building the image and generating the modulus, you can mint credentials like so:

`docker run crypto:latest ./mint_credentials -m <output-of-modulus-generation>`