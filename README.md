# Readme
## To build the project
### Requirements
Docker
### Script
Run provided script
```
bin/build_image.sh
```
## To run the project locally
```
bin/run_server.sh
```
## To push image to remote docker repository
```
bin/push_image.sh {repository_name} {tag_name}
```