#!/bin/bash
docker kill crypto
docker kill dynamodb-local

docker run -d --name crypto --rm -v "$(pwd)"/server:/app/server --network="host" crypto:latest flask run --host=0.0.0.0
docker run -d --name dynamodb-local --rm --network="host" amazon/dynamodb-local:latest
eval $(aws-env development)
aws dynamodb create-table \
  --attribute-definitions AttributeName=VoterId,AttributeType=S \
  --table-name VoterAuthenticationTokens \
  --key-schema AttributeName=VoterId,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
  --profile development \
  --endpoint-url http://localhost:8000 \
  --region eu-west-2

aws dynamodb list-tables --endpoint-url http://localhost:8000 --profile development --region eu-west-2

request_data=$(cat << EndOfMessage
{
  "VoterAuthenticationTokens": [
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_1@domain.com"}, "VoterToken": {"S": "test_1"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_2@domain.com"}, "VoterToken": {"S": "test_2"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_3@domain.com"}, "VoterToken": {"S": "test_3"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_4@domain.com"}, "VoterToken": {"S": "test_4"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_5@domain.com"}, "VoterToken": {"S": "test_5"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_6@domain.com"}, "VoterToken": {"S": "test_6"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_7@domain.com"}, "VoterToken": {"S": "test_7"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_8@domain.com"}, "VoterToken": {"S": "test_8"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_9@domain.com"}, "VoterToken": {"S": "test_9"} } } },
    { "PutRequest": { "Item" : { "VoterId": {"S": "test_10@domain.com"}, "VoterToken": {"S": "test_10"} } } }
  ]
}
EndOfMessage
)
echo "${request_data}"
aws dynamodb batch-write-item --request-items "${request_data}" --endpoint-url http://localhost:8000 --profile development --region eu-west-2
