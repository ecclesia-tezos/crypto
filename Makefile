# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.17

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ivo/projects/e-cclesia/front-end/crypto

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ivo/projects/e-cclesia/front-end/crypto

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/local/bin/cmake --regenerate-during-build -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "No interactive CMake dialog available..."
	/usr/local/bin/cmake -E echo No\ interactive\ CMake\ dialog\ available.
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/ivo/projects/e-cclesia/front-end/crypto/CMakeFiles /home/ivo/projects/e-cclesia/front-end/crypto/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/ivo/projects/e-cclesia/front-end/crypto/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named ZEROCOIN_A

# Build rule for target.
ZEROCOIN_A: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ZEROCOIN_A
.PHONY : ZEROCOIN_A

# fast build rule for target.
ZEROCOIN_A/fast:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/build
.PHONY : ZEROCOIN_A/fast

#=============================================================================
# Target rules for targets named mint_credentials

# Build rule for target.
mint_credentials: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 mint_credentials
.PHONY : mint_credentials

# fast build rule for target.
mint_credentials/fast:
	$(MAKE) -f CMakeFiles/mint_credentials.dir/build.make CMakeFiles/mint_credentials.dir/build
.PHONY : mint_credentials/fast

#=============================================================================
# Target rules for targets named UTIL_A

# Build rule for target.
UTIL_A: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 UTIL_A
.PHONY : UTIL_A

# fast build rule for target.
UTIL_A/fast:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/build
.PHONY : UTIL_A/fast

#=============================================================================
# Target rules for targets named generate_modulus

# Build rule for target.
generate_modulus: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 generate_modulus
.PHONY : generate_modulus

# fast build rule for target.
generate_modulus/fast:
	$(MAKE) -f CMakeFiles/generate_modulus.dir/build.make CMakeFiles/generate_modulus.dir/build
.PHONY : generate_modulus/fast

#=============================================================================
# Target rules for targets named BITCOIN_CRYPTO_A

# Build rule for target.
BITCOIN_CRYPTO_A: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 BITCOIN_CRYPTO_A
.PHONY : BITCOIN_CRYPTO_A

# fast build rule for target.
BITCOIN_CRYPTO_A/fast:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/build
.PHONY : BITCOIN_CRYPTO_A/fast

#=============================================================================
# Target rules for targets named libsecp256k1

# Build rule for target.
libsecp256k1: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 libsecp256k1
.PHONY : libsecp256k1

# fast build rule for target.
libsecp256k1/fast:
	$(MAKE) -f CMakeFiles/libsecp256k1.dir/build.make CMakeFiles/libsecp256k1.dir/build
.PHONY : libsecp256k1/fast

#=============================================================================
# Target rules for targets named COMMON_A

# Build rule for target.
COMMON_A: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 COMMON_A
.PHONY : COMMON_A

# fast build rule for target.
COMMON_A/fast:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/build
.PHONY : COMMON_A/fast

allocators.o: allocators.cpp.o

.PHONY : allocators.o

# target to build an object file
allocators.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/allocators.cpp.o
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/allocators.cpp.o
.PHONY : allocators.cpp.o

allocators.i: allocators.cpp.i

.PHONY : allocators.i

# target to preprocess a source file
allocators.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/allocators.cpp.i
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/allocators.cpp.i
.PHONY : allocators.cpp.i

allocators.s: allocators.cpp.s

.PHONY : allocators.s

# target to generate assembly for a file
allocators.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/allocators.cpp.s
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/allocators.cpp.s
.PHONY : allocators.cpp.s

amount.o: amount.cpp.o

.PHONY : amount.o

# target to build an object file
amount.cpp.o:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/amount.cpp.o
.PHONY : amount.cpp.o

amount.i: amount.cpp.i

.PHONY : amount.i

# target to preprocess a source file
amount.cpp.i:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/amount.cpp.i
.PHONY : amount.cpp.i

amount.s: amount.cpp.s

.PHONY : amount.s

# target to generate assembly for a file
amount.cpp.s:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/amount.cpp.s
.PHONY : amount.cpp.s

chainparamsbase.o: chainparamsbase.cpp.o

.PHONY : chainparamsbase.o

# target to build an object file
chainparamsbase.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/chainparamsbase.cpp.o
.PHONY : chainparamsbase.cpp.o

chainparamsbase.i: chainparamsbase.cpp.i

.PHONY : chainparamsbase.i

# target to preprocess a source file
chainparamsbase.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/chainparamsbase.cpp.i
.PHONY : chainparamsbase.cpp.i

chainparamsbase.s: chainparamsbase.cpp.s

.PHONY : chainparamsbase.s

# target to generate assembly for a file
chainparamsbase.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/chainparamsbase.cpp.s
.PHONY : chainparamsbase.cpp.s

crypto/chacha20.o: crypto/chacha20.cpp.o

.PHONY : crypto/chacha20.o

# target to build an object file
crypto/chacha20.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/chacha20.cpp.o
.PHONY : crypto/chacha20.cpp.o

crypto/chacha20.i: crypto/chacha20.cpp.i

.PHONY : crypto/chacha20.i

# target to preprocess a source file
crypto/chacha20.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/chacha20.cpp.i
.PHONY : crypto/chacha20.cpp.i

crypto/chacha20.s: crypto/chacha20.cpp.s

.PHONY : crypto/chacha20.s

# target to generate assembly for a file
crypto/chacha20.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/chacha20.cpp.s
.PHONY : crypto/chacha20.cpp.s

crypto/hmac_sha512.o: crypto/hmac_sha512.cpp.o

.PHONY : crypto/hmac_sha512.o

# target to build an object file
crypto/hmac_sha512.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/hmac_sha512.cpp.o
.PHONY : crypto/hmac_sha512.cpp.o

crypto/hmac_sha512.i: crypto/hmac_sha512.cpp.i

.PHONY : crypto/hmac_sha512.i

# target to preprocess a source file
crypto/hmac_sha512.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/hmac_sha512.cpp.i
.PHONY : crypto/hmac_sha512.cpp.i

crypto/hmac_sha512.s: crypto/hmac_sha512.cpp.s

.PHONY : crypto/hmac_sha512.s

# target to generate assembly for a file
crypto/hmac_sha512.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/hmac_sha512.cpp.s
.PHONY : crypto/hmac_sha512.cpp.s

crypto/ripemd160.o: crypto/ripemd160.cpp.o

.PHONY : crypto/ripemd160.o

# target to build an object file
crypto/ripemd160.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/ripemd160.cpp.o
.PHONY : crypto/ripemd160.cpp.o

crypto/ripemd160.i: crypto/ripemd160.cpp.i

.PHONY : crypto/ripemd160.i

# target to preprocess a source file
crypto/ripemd160.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/ripemd160.cpp.i
.PHONY : crypto/ripemd160.cpp.i

crypto/ripemd160.s: crypto/ripemd160.cpp.s

.PHONY : crypto/ripemd160.s

# target to generate assembly for a file
crypto/ripemd160.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/ripemd160.cpp.s
.PHONY : crypto/ripemd160.cpp.s

crypto/scrypt.o: crypto/scrypt.cpp.o

.PHONY : crypto/scrypt.o

# target to build an object file
crypto/scrypt.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/scrypt.cpp.o
.PHONY : crypto/scrypt.cpp.o

crypto/scrypt.i: crypto/scrypt.cpp.i

.PHONY : crypto/scrypt.i

# target to preprocess a source file
crypto/scrypt.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/scrypt.cpp.i
.PHONY : crypto/scrypt.cpp.i

crypto/scrypt.s: crypto/scrypt.cpp.s

.PHONY : crypto/scrypt.s

# target to generate assembly for a file
crypto/scrypt.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/scrypt.cpp.s
.PHONY : crypto/scrypt.cpp.s

crypto/sha256.o: crypto/sha256.cpp.o

.PHONY : crypto/sha256.o

# target to build an object file
crypto/sha256.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha256.cpp.o
.PHONY : crypto/sha256.cpp.o

crypto/sha256.i: crypto/sha256.cpp.i

.PHONY : crypto/sha256.i

# target to preprocess a source file
crypto/sha256.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha256.cpp.i
.PHONY : crypto/sha256.cpp.i

crypto/sha256.s: crypto/sha256.cpp.s

.PHONY : crypto/sha256.s

# target to generate assembly for a file
crypto/sha256.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha256.cpp.s
.PHONY : crypto/sha256.cpp.s

crypto/sha512.o: crypto/sha512.cpp.o

.PHONY : crypto/sha512.o

# target to build an object file
crypto/sha512.cpp.o:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha512.cpp.o
.PHONY : crypto/sha512.cpp.o

crypto/sha512.i: crypto/sha512.cpp.i

.PHONY : crypto/sha512.i

# target to preprocess a source file
crypto/sha512.cpp.i:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha512.cpp.i
.PHONY : crypto/sha512.cpp.i

crypto/sha512.s: crypto/sha512.cpp.s

.PHONY : crypto/sha512.s

# target to generate assembly for a file
crypto/sha512.cpp.s:
	$(MAKE) -f CMakeFiles/BITCOIN_CRYPTO_A.dir/build.make CMakeFiles/BITCOIN_CRYPTO_A.dir/crypto/sha512.cpp.s
.PHONY : crypto/sha512.cpp.s

generate_modulus.o: generate_modulus.cpp.o

.PHONY : generate_modulus.o

# target to build an object file
generate_modulus.cpp.o:
	$(MAKE) -f CMakeFiles/generate_modulus.dir/build.make CMakeFiles/generate_modulus.dir/generate_modulus.cpp.o
.PHONY : generate_modulus.cpp.o

generate_modulus.i: generate_modulus.cpp.i

.PHONY : generate_modulus.i

# target to preprocess a source file
generate_modulus.cpp.i:
	$(MAKE) -f CMakeFiles/generate_modulus.dir/build.make CMakeFiles/generate_modulus.dir/generate_modulus.cpp.i
.PHONY : generate_modulus.cpp.i

generate_modulus.s: generate_modulus.cpp.s

.PHONY : generate_modulus.s

# target to generate assembly for a file
generate_modulus.cpp.s:
	$(MAKE) -f CMakeFiles/generate_modulus.dir/build.make CMakeFiles/generate_modulus.dir/generate_modulus.cpp.s
.PHONY : generate_modulus.cpp.s

hash.o: hash.cpp.o

.PHONY : hash.o

# target to build an object file
hash.cpp.o:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/hash.cpp.o
.PHONY : hash.cpp.o

hash.i: hash.cpp.i

.PHONY : hash.i

# target to preprocess a source file
hash.cpp.i:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/hash.cpp.i
.PHONY : hash.cpp.i

hash.s: hash.cpp.s

.PHONY : hash.s

# target to generate assembly for a file
hash.cpp.s:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/hash.cpp.s
.PHONY : hash.cpp.s

key.o: key.cpp.o

.PHONY : key.o

# target to build an object file
key.cpp.o:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/key.cpp.o
.PHONY : key.cpp.o

key.i: key.cpp.i

.PHONY : key.i

# target to preprocess a source file
key.cpp.i:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/key.cpp.i
.PHONY : key.cpp.i

key.s: key.cpp.s

.PHONY : key.s

# target to generate assembly for a file
key.cpp.s:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/key.cpp.s
.PHONY : key.cpp.s

libzerocoin/Accumulator.o: libzerocoin/Accumulator.cpp.o

.PHONY : libzerocoin/Accumulator.o

# target to build an object file
libzerocoin/Accumulator.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Accumulator.cpp.o
.PHONY : libzerocoin/Accumulator.cpp.o

libzerocoin/Accumulator.i: libzerocoin/Accumulator.cpp.i

.PHONY : libzerocoin/Accumulator.i

# target to preprocess a source file
libzerocoin/Accumulator.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Accumulator.cpp.i
.PHONY : libzerocoin/Accumulator.cpp.i

libzerocoin/Accumulator.s: libzerocoin/Accumulator.cpp.s

.PHONY : libzerocoin/Accumulator.s

# target to generate assembly for a file
libzerocoin/Accumulator.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Accumulator.cpp.s
.PHONY : libzerocoin/Accumulator.cpp.s

libzerocoin/Coin.o: libzerocoin/Coin.cpp.o

.PHONY : libzerocoin/Coin.o

# target to build an object file
libzerocoin/Coin.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Coin.cpp.o
.PHONY : libzerocoin/Coin.cpp.o

libzerocoin/Coin.i: libzerocoin/Coin.cpp.i

.PHONY : libzerocoin/Coin.i

# target to preprocess a source file
libzerocoin/Coin.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Coin.cpp.i
.PHONY : libzerocoin/Coin.cpp.i

libzerocoin/Coin.s: libzerocoin/Coin.cpp.s

.PHONY : libzerocoin/Coin.s

# target to generate assembly for a file
libzerocoin/Coin.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Coin.cpp.s
.PHONY : libzerocoin/Coin.cpp.s

libzerocoin/CoinRandomnessSchnorrSignature.o: libzerocoin/CoinRandomnessSchnorrSignature.cpp.o

.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.o

# target to build an object file
libzerocoin/CoinRandomnessSchnorrSignature.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinRandomnessSchnorrSignature.cpp.o
.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.cpp.o

libzerocoin/CoinRandomnessSchnorrSignature.i: libzerocoin/CoinRandomnessSchnorrSignature.cpp.i

.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.i

# target to preprocess a source file
libzerocoin/CoinRandomnessSchnorrSignature.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinRandomnessSchnorrSignature.cpp.i
.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.cpp.i

libzerocoin/CoinRandomnessSchnorrSignature.s: libzerocoin/CoinRandomnessSchnorrSignature.cpp.s

.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.s

# target to generate assembly for a file
libzerocoin/CoinRandomnessSchnorrSignature.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinRandomnessSchnorrSignature.cpp.s
.PHONY : libzerocoin/CoinRandomnessSchnorrSignature.cpp.s

libzerocoin/CoinSpend.o: libzerocoin/CoinSpend.cpp.o

.PHONY : libzerocoin/CoinSpend.o

# target to build an object file
libzerocoin/CoinSpend.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinSpend.cpp.o
.PHONY : libzerocoin/CoinSpend.cpp.o

libzerocoin/CoinSpend.i: libzerocoin/CoinSpend.cpp.i

.PHONY : libzerocoin/CoinSpend.i

# target to preprocess a source file
libzerocoin/CoinSpend.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinSpend.cpp.i
.PHONY : libzerocoin/CoinSpend.cpp.i

libzerocoin/CoinSpend.s: libzerocoin/CoinSpend.cpp.s

.PHONY : libzerocoin/CoinSpend.s

# target to generate assembly for a file
libzerocoin/CoinSpend.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/CoinSpend.cpp.s
.PHONY : libzerocoin/CoinSpend.cpp.s

libzerocoin/Denominations.o: libzerocoin/Denominations.cpp.o

.PHONY : libzerocoin/Denominations.o

# target to build an object file
libzerocoin/Denominations.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Denominations.cpp.o
.PHONY : libzerocoin/Denominations.cpp.o

libzerocoin/Denominations.i: libzerocoin/Denominations.cpp.i

.PHONY : libzerocoin/Denominations.i

# target to preprocess a source file
libzerocoin/Denominations.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Denominations.cpp.i
.PHONY : libzerocoin/Denominations.cpp.i

libzerocoin/Denominations.s: libzerocoin/Denominations.cpp.s

.PHONY : libzerocoin/Denominations.s

# target to generate assembly for a file
libzerocoin/Denominations.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Denominations.cpp.s
.PHONY : libzerocoin/Denominations.cpp.s

libzerocoin/ParamGeneration.o: libzerocoin/ParamGeneration.cpp.o

.PHONY : libzerocoin/ParamGeneration.o

# target to build an object file
libzerocoin/ParamGeneration.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/ParamGeneration.cpp.o
.PHONY : libzerocoin/ParamGeneration.cpp.o

libzerocoin/ParamGeneration.i: libzerocoin/ParamGeneration.cpp.i

.PHONY : libzerocoin/ParamGeneration.i

# target to preprocess a source file
libzerocoin/ParamGeneration.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/ParamGeneration.cpp.i
.PHONY : libzerocoin/ParamGeneration.cpp.i

libzerocoin/ParamGeneration.s: libzerocoin/ParamGeneration.cpp.s

.PHONY : libzerocoin/ParamGeneration.s

# target to generate assembly for a file
libzerocoin/ParamGeneration.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/ParamGeneration.cpp.s
.PHONY : libzerocoin/ParamGeneration.cpp.s

libzerocoin/Params.o: libzerocoin/Params.cpp.o

.PHONY : libzerocoin/Params.o

# target to build an object file
libzerocoin/Params.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Params.cpp.o
.PHONY : libzerocoin/Params.cpp.o

libzerocoin/Params.i: libzerocoin/Params.cpp.i

.PHONY : libzerocoin/Params.i

# target to preprocess a source file
libzerocoin/Params.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Params.cpp.i
.PHONY : libzerocoin/Params.cpp.i

libzerocoin/Params.s: libzerocoin/Params.cpp.s

.PHONY : libzerocoin/Params.s

# target to generate assembly for a file
libzerocoin/Params.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/Params.cpp.s
.PHONY : libzerocoin/Params.cpp.s

libzerocoin/bignum.o: libzerocoin/bignum.cpp.o

.PHONY : libzerocoin/bignum.o

# target to build an object file
libzerocoin/bignum.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum.cpp.o
.PHONY : libzerocoin/bignum.cpp.o

libzerocoin/bignum.i: libzerocoin/bignum.cpp.i

.PHONY : libzerocoin/bignum.i

# target to preprocess a source file
libzerocoin/bignum.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum.cpp.i
.PHONY : libzerocoin/bignum.cpp.i

libzerocoin/bignum.s: libzerocoin/bignum.cpp.s

.PHONY : libzerocoin/bignum.s

# target to generate assembly for a file
libzerocoin/bignum.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum.cpp.s
.PHONY : libzerocoin/bignum.cpp.s

libzerocoin/bignum_openssl.o: libzerocoin/bignum_openssl.cpp.o

.PHONY : libzerocoin/bignum_openssl.o

# target to build an object file
libzerocoin/bignum_openssl.cpp.o:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum_openssl.cpp.o
.PHONY : libzerocoin/bignum_openssl.cpp.o

libzerocoin/bignum_openssl.i: libzerocoin/bignum_openssl.cpp.i

.PHONY : libzerocoin/bignum_openssl.i

# target to preprocess a source file
libzerocoin/bignum_openssl.cpp.i:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum_openssl.cpp.i
.PHONY : libzerocoin/bignum_openssl.cpp.i

libzerocoin/bignum_openssl.s: libzerocoin/bignum_openssl.cpp.s

.PHONY : libzerocoin/bignum_openssl.s

# target to generate assembly for a file
libzerocoin/bignum_openssl.cpp.s:
	$(MAKE) -f CMakeFiles/ZEROCOIN_A.dir/build.make CMakeFiles/ZEROCOIN_A.dir/libzerocoin/bignum_openssl.cpp.s
.PHONY : libzerocoin/bignum_openssl.cpp.s

mint_credentials.o: mint_credentials.cpp.o

.PHONY : mint_credentials.o

# target to build an object file
mint_credentials.cpp.o:
	$(MAKE) -f CMakeFiles/mint_credentials.dir/build.make CMakeFiles/mint_credentials.dir/mint_credentials.cpp.o
.PHONY : mint_credentials.cpp.o

mint_credentials.i: mint_credentials.cpp.i

.PHONY : mint_credentials.i

# target to preprocess a source file
mint_credentials.cpp.i:
	$(MAKE) -f CMakeFiles/mint_credentials.dir/build.make CMakeFiles/mint_credentials.dir/mint_credentials.cpp.i
.PHONY : mint_credentials.cpp.i

mint_credentials.s: mint_credentials.cpp.s

.PHONY : mint_credentials.s

# target to generate assembly for a file
mint_credentials.cpp.s:
	$(MAKE) -f CMakeFiles/mint_credentials.dir/build.make CMakeFiles/mint_credentials.dir/mint_credentials.cpp.s
.PHONY : mint_credentials.cpp.s

pubkey.o: pubkey.cpp.o

.PHONY : pubkey.o

# target to build an object file
pubkey.cpp.o:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/pubkey.cpp.o
.PHONY : pubkey.cpp.o

pubkey.i: pubkey.cpp.i

.PHONY : pubkey.i

# target to preprocess a source file
pubkey.cpp.i:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/pubkey.cpp.i
.PHONY : pubkey.cpp.i

pubkey.s: pubkey.cpp.s

.PHONY : pubkey.s

# target to generate assembly for a file
pubkey.cpp.s:
	$(MAKE) -f CMakeFiles/COMMON_A.dir/build.make CMakeFiles/COMMON_A.dir/pubkey.cpp.s
.PHONY : pubkey.cpp.s

random.o: random.cpp.o

.PHONY : random.o

# target to build an object file
random.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/random.cpp.o
.PHONY : random.cpp.o

random.i: random.cpp.i

.PHONY : random.i

# target to preprocess a source file
random.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/random.cpp.i
.PHONY : random.cpp.i

random.s: random.cpp.s

.PHONY : random.s

# target to generate assembly for a file
random.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/random.cpp.s
.PHONY : random.cpp.s

support/cleanse.o: support/cleanse.cpp.o

.PHONY : support/cleanse.o

# target to build an object file
support/cleanse.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/support/cleanse.cpp.o
.PHONY : support/cleanse.cpp.o

support/cleanse.i: support/cleanse.cpp.i

.PHONY : support/cleanse.i

# target to preprocess a source file
support/cleanse.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/support/cleanse.cpp.i
.PHONY : support/cleanse.cpp.i

support/cleanse.s: support/cleanse.cpp.s

.PHONY : support/cleanse.s

# target to generate assembly for a file
support/cleanse.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/support/cleanse.cpp.s
.PHONY : support/cleanse.cpp.s

uint256.o: uint256.cpp.o

.PHONY : uint256.o

# target to build an object file
uint256.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/uint256.cpp.o
.PHONY : uint256.cpp.o

uint256.i: uint256.cpp.i

.PHONY : uint256.i

# target to preprocess a source file
uint256.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/uint256.cpp.i
.PHONY : uint256.cpp.i

uint256.s: uint256.cpp.s

.PHONY : uint256.s

# target to generate assembly for a file
uint256.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/uint256.cpp.s
.PHONY : uint256.cpp.s

util.o: util.cpp.o

.PHONY : util.o

# target to build an object file
util.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util.cpp.o
.PHONY : util.cpp.o

util.i: util.cpp.i

.PHONY : util.i

# target to preprocess a source file
util.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util.cpp.i
.PHONY : util.cpp.i

util.s: util.cpp.s

.PHONY : util.s

# target to generate assembly for a file
util.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util.cpp.s
.PHONY : util.cpp.s

util/threadnames.o: util/threadnames.cpp.o

.PHONY : util/threadnames.o

# target to build an object file
util/threadnames.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util/threadnames.cpp.o
.PHONY : util/threadnames.cpp.o

util/threadnames.i: util/threadnames.cpp.i

.PHONY : util/threadnames.i

# target to preprocess a source file
util/threadnames.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util/threadnames.cpp.i
.PHONY : util/threadnames.cpp.i

util/threadnames.s: util/threadnames.cpp.s

.PHONY : util/threadnames.s

# target to generate assembly for a file
util/threadnames.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/util/threadnames.cpp.s
.PHONY : util/threadnames.cpp.s

utilstrencodings.o: utilstrencodings.cpp.o

.PHONY : utilstrencodings.o

# target to build an object file
utilstrencodings.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utilstrencodings.cpp.o
.PHONY : utilstrencodings.cpp.o

utilstrencodings.i: utilstrencodings.cpp.i

.PHONY : utilstrencodings.i

# target to preprocess a source file
utilstrencodings.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utilstrencodings.cpp.i
.PHONY : utilstrencodings.cpp.i

utilstrencodings.s: utilstrencodings.cpp.s

.PHONY : utilstrencodings.s

# target to generate assembly for a file
utilstrencodings.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utilstrencodings.cpp.s
.PHONY : utilstrencodings.cpp.s

utiltime.o: utiltime.cpp.o

.PHONY : utiltime.o

# target to build an object file
utiltime.cpp.o:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utiltime.cpp.o
.PHONY : utiltime.cpp.o

utiltime.i: utiltime.cpp.i

.PHONY : utiltime.i

# target to preprocess a source file
utiltime.cpp.i:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utiltime.cpp.i
.PHONY : utiltime.cpp.i

utiltime.s: utiltime.cpp.s

.PHONY : utiltime.s

# target to generate assembly for a file
utiltime.cpp.s:
	$(MAKE) -f CMakeFiles/UTIL_A.dir/build.make CMakeFiles/UTIL_A.dir/utiltime.cpp.s
.PHONY : utiltime.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... libsecp256k1"
	@echo "... BITCOIN_CRYPTO_A"
	@echo "... COMMON_A"
	@echo "... UTIL_A"
	@echo "... ZEROCOIN_A"
	@echo "... generate_modulus"
	@echo "... mint_credentials"
	@echo "... allocators.o"
	@echo "... allocators.i"
	@echo "... allocators.s"
	@echo "... amount.o"
	@echo "... amount.i"
	@echo "... amount.s"
	@echo "... chainparamsbase.o"
	@echo "... chainparamsbase.i"
	@echo "... chainparamsbase.s"
	@echo "... crypto/chacha20.o"
	@echo "... crypto/chacha20.i"
	@echo "... crypto/chacha20.s"
	@echo "... crypto/hmac_sha512.o"
	@echo "... crypto/hmac_sha512.i"
	@echo "... crypto/hmac_sha512.s"
	@echo "... crypto/ripemd160.o"
	@echo "... crypto/ripemd160.i"
	@echo "... crypto/ripemd160.s"
	@echo "... crypto/scrypt.o"
	@echo "... crypto/scrypt.i"
	@echo "... crypto/scrypt.s"
	@echo "... crypto/sha256.o"
	@echo "... crypto/sha256.i"
	@echo "... crypto/sha256.s"
	@echo "... crypto/sha512.o"
	@echo "... crypto/sha512.i"
	@echo "... crypto/sha512.s"
	@echo "... generate_modulus.o"
	@echo "... generate_modulus.i"
	@echo "... generate_modulus.s"
	@echo "... hash.o"
	@echo "... hash.i"
	@echo "... hash.s"
	@echo "... key.o"
	@echo "... key.i"
	@echo "... key.s"
	@echo "... libzerocoin/Accumulator.o"
	@echo "... libzerocoin/Accumulator.i"
	@echo "... libzerocoin/Accumulator.s"
	@echo "... libzerocoin/Coin.o"
	@echo "... libzerocoin/Coin.i"
	@echo "... libzerocoin/Coin.s"
	@echo "... libzerocoin/CoinRandomnessSchnorrSignature.o"
	@echo "... libzerocoin/CoinRandomnessSchnorrSignature.i"
	@echo "... libzerocoin/CoinRandomnessSchnorrSignature.s"
	@echo "... libzerocoin/CoinSpend.o"
	@echo "... libzerocoin/CoinSpend.i"
	@echo "... libzerocoin/CoinSpend.s"
	@echo "... libzerocoin/Denominations.o"
	@echo "... libzerocoin/Denominations.i"
	@echo "... libzerocoin/Denominations.s"
	@echo "... libzerocoin/ParamGeneration.o"
	@echo "... libzerocoin/ParamGeneration.i"
	@echo "... libzerocoin/ParamGeneration.s"
	@echo "... libzerocoin/Params.o"
	@echo "... libzerocoin/Params.i"
	@echo "... libzerocoin/Params.s"
	@echo "... libzerocoin/bignum.o"
	@echo "... libzerocoin/bignum.i"
	@echo "... libzerocoin/bignum.s"
	@echo "... libzerocoin/bignum_openssl.o"
	@echo "... libzerocoin/bignum_openssl.i"
	@echo "... libzerocoin/bignum_openssl.s"
	@echo "... mint_credentials.o"
	@echo "... mint_credentials.i"
	@echo "... mint_credentials.s"
	@echo "... pubkey.o"
	@echo "... pubkey.i"
	@echo "... pubkey.s"
	@echo "... random.o"
	@echo "... random.i"
	@echo "... random.s"
	@echo "... support/cleanse.o"
	@echo "... support/cleanse.i"
	@echo "... support/cleanse.s"
	@echo "... uint256.o"
	@echo "... uint256.i"
	@echo "... uint256.s"
	@echo "... util.o"
	@echo "... util.i"
	@echo "... util.s"
	@echo "... util/threadnames.o"
	@echo "... util/threadnames.i"
	@echo "... util/threadnames.s"
	@echo "... utilstrencodings.o"
	@echo "... utilstrencodings.i"
	@echo "... utilstrencodings.s"
	@echo "... utiltime.o"
	@echo "... utiltime.i"
	@echo "... utiltime.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

