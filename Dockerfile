FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    build-essential \
    cmake \
    libboost-all-dev \
    libssl1.0-dev \
    libgmp3-dev \
    autoconf \
    software-properties-common

RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt-get update && apt-get install -y \
    python3.7 \
    python3-pip

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/

COPY . /app
WORKDIR /app/zkp/build
RUN cmake ../libzerocoin 
RUN cmake --build .

WORKDIR /app/server
RUN pip3 install -r requirements.txt

# TODO: use a production WSGI server
# instructions here: https://pythonise.com/series/learning-flask/building-a-flask-app-with-docker-compose
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV FLASK_NAME=crypto_service.py
# you need an aws credential setup named [development] with these access keys set up in your environment
ENV AWS_ACCESS_KEY_ID=RANDOM
ENV AWS_SECRET_ACCESS_KEY=RANDOM
ENV ENVIRONMENT=development
EXPOSE 5000
