#include "libzerocoin/Coin.h"
#include "libzerocoin/Accumulator.h"
#include "libzerocoin/CoinSpend.h"
#include "streams.h"
#include "base64.h"

#include <string>
#include <iostream>
#include <fstream>
#include <exception>

using namespace std;
using namespace libzerocoin;

int main(int argc, char **argv)
{
    static CBigNum modulus(0);
    string serializedVote;

    int i = argc;
    char **j = argv;
    while ((i > 1) && (j[1][0] == '-'))
    {
        switch (j[1][1])
        {
        case 'm':
            modulus.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'v':
            serializedVote = j[2];
            ++j;
            --i;
            break;

        default:
            ++j;
            --i;
            break;
        }

        ++j;
        --i;
    }

    ZerocoinParams params(modulus); 
    CoinDenomination denomination = IntToZerocoinDenomination(1);

    // parse CoinSpend from input
    string decodedStr = base64_decode(serializedVote);
    vector<char> vctr(decodedStr.begin(), decodedStr.end());
    CDataStream ds(vctr, SER_NETWORK, PROTOCOL_VERSION);
    CoinSpend dSpend(&params, ds);


    // deserialize and accumulate from input
    i = argc;
    j = argv;
    Accumulator accumulator(&params, denomination);
    CBigNum publicCommitment(0);

    while ((i > 1) && (j[1][0] == '-'))
    {
        switch (j[1][1])
        {
        case 'a':
            publicCommitment.SetHex(j[2]);
            accumulator += PublicCoin(&params, publicCommitment, denomination);
            ++j;
            --i;
            break;

        default:
            ++j;
            --i;
            break;
        }

        ++j;
        --i;
    }

    cout << dSpend.Verify(accumulator) << endl;
    cout << dSpend.getVote().GetHex() << endl;
}


