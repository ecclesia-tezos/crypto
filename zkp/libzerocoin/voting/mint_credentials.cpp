#include "libzerocoin/Coin.h"

#include <string>
#include <iostream>
#include <fstream>
#include <exception>

using namespace std;
using namespace libzerocoin;

void usage()
{
    printf("Usage:\n");
    printf(" -m <modulus in hex>\n");

    exit (8);
}

int main(int argc, char **argv)
{
    static CBigNum modulus(0);

    while ((argc > 1) && (argv[1][0] == '-'))
    {
        switch (argv[1][1])
        {
        case 'm':
            modulus.SetHex(argv[2]);
            ++argv;
            --argc;
            break;

        case 'h':
            usage();
            break;

        default:
            printf("Wrong Argument: %s\n", argv[1]);
            usage();
            break;
        }

        ++argv;
        --argc;
    }

    ZerocoinParams params(modulus); 

    CoinDenomination denomination = IntToZerocoinDenomination(1);

    PrivateCoin coin(&params, denomination);

    CBigNum serialNumber = coin.getSerialNumber();
    CBigNum randomness = coin.getRandomness();
    
    cout << coin.getPublicCoin().getValue().GetHex() << endl;
    cout << serialNumber.GetHex() << endl;
    cout << randomness.GetHex() << endl;
}
