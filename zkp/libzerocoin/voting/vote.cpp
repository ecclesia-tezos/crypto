#include "libzerocoin/Coin.h"
#include "libzerocoin/Accumulator.h"
#include "libzerocoin/CoinSpend.h"
#include "streams.h"
#include "base64.h"

#include <string>
#include <iostream>
#include <fstream>
#include <exception>

using namespace std;
using namespace libzerocoin;

int main(int argc, char **argv)
{
    static CBigNum modulus(0);
    static CBigNum serialNumber(0);
    static CBigNum randomness(0);
    static CBigNum commitment(0);
    static CBigNum vote(0);

    int i = argc;
    char **j = argv;
    while ((i > 1) && (j[1][0] == '-'))
    {
        switch (j[1][1])
        {
        case 'm':
            modulus.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 's':
            serialNumber.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'r':
            randomness.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'c':
            commitment.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'p':
            commitment.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'v':
            vote.SetHex(j[2]);
            ++j;
            --i;
            break;

        case 'a':
            ++j;
            --i;
            break;

        default:
            printf("Wrong Argument: %s\n", j[1]);
            break;
        }

        ++j;
        --i;
    }

    // deserialize user's PrivateCoin from input
    ZerocoinParams params(modulus); 
    CoinDenomination denomination = IntToZerocoinDenomination(1);

    PrivateCoin coin(&params, denomination, serialNumber, randomness, commitment);

    // deserialize and accumulate from input
    i = argc;
    j = argv;
    Accumulator accumulator(&params, denomination);
    CBigNum otherCommitment(0);

    while ((i > 1) && (j[1][0] == '-'))
    {
        switch (j[1][1])
        {
        case 'a':
            otherCommitment.SetHex(j[2]);
            accumulator += PublicCoin(&params, otherCommitment, denomination);
            ++j;
            --i;
            break;

        default:
            ++j;
            --i;
            break;
        }

        ++j;
        --i;
    }

    AccumulatorWitness witness(&params, accumulator, coin.getPublicCoin());
    accumulator += coin.getPublicCoin();

    //TODO the last argument should be the TLE
    CoinSpend spend(&params, coin, accumulator, 0, witness, vote);
    CDataStream ss(SER_NETWORK, PROTOCOL_VERSION);
    
    ss << spend;

    string str = ss.str();
    string encodedStr = base64_encode((unsigned char*) str.c_str(), str.length());
    cout << encodedStr << endl;
}

